package com.zerga.morales.reto.bcp.exchange.controller.request;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ExchangeCreateRequest {

    @Length(min = 3, max = 3)
    @NotNull
    private String coinSource;

    @Length(min = 3, max = 3)
    @NotNull
    private String coinDestiny;

    @DecimalMin("0.0")
    private Double rate;
}
