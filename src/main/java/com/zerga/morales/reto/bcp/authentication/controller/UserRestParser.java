package com.zerga.morales.reto.bcp.authentication.controller;

import com.zerga.morales.reto.bcp.authentication.business.domain.User;
import com.zerga.morales.reto.bcp.authentication.controller.request.GenerateTokenRequest;
import com.zerga.morales.reto.bcp.authentication.controller.response.UserResponse;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRestParser {

    public User convertRequestToDomain(GenerateTokenRequest request) {
        return Optional.ofNullable(request)
                .map(element -> User.builder()
                        .user(element.getUser())
                        .password(element.getPassword())
                        .build())
                .orElse(null);
    }

    public UserResponse convertDomainToResponse(User user, String token) {
        return UserResponse.builder()
                .user(user.getUser())
                .token(token)
                .build();
    }
}
