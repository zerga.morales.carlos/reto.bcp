package com.zerga.morales.reto.bcp.exchange.exception;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;

public class ExchangeRateNotFoundException extends BaseException {

    private final Exchange exchange;

    public ExchangeRateNotFoundException(Exchange domain) {
        super();
        this.exchange = domain;
    }

    @Override
    public String getCode() {
        return "EX_RT_01";
    }

    @Override
    public String getError() {
        return String.format("Currently conversion from '%s' to '%s' is notfound", this.exchange.getCoinSource(), this.exchange.getCoinDestiny());
    }
}
