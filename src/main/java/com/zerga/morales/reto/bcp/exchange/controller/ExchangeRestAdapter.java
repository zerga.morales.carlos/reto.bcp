package com.zerga.morales.reto.bcp.exchange.controller;

import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.business.output.ExchangeService;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeCreateRequest;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeDeleteRequest;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import com.zerga.morales.reto.bcp.exchange.controller.response.ExchangeResponse;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/core/exchange")
@Slf4j
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.GET})
@Validated
public class ExchangeRestAdapter {

    private final ExchangeService exchangeService;

    private final ExchangeRestParser exchangeRestParser;

    public ExchangeRestAdapter(ExchangeService exchangeService, ExchangeRestParser exchangeRestParser) {
        this.exchangeService = exchangeService;
        this.exchangeRestParser = exchangeRestParser;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<ExchangeResponse> createExchangeCoin(
            @Valid @RequestBody final List<ExchangeCreateRequest> exchangeCreateRequest
    ) {

        return exchangeCreateRequest.stream()
                .map(this.exchangeRestParser::convertRequestToDomain)
                .map(this.exchangeService::createExchange)
                .map(this.exchangeRestParser::convertDomainToResponse)
                .collect(Collectors.toList());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntityList<ExchangeResponse> createExchangeCoin(
            @Valid final ExchangeQueryRequest exchangeQueryRequest
    ) {

        return this.exchangeRestParser.convertDomainToResponse(
                this.exchangeService.list(exchangeQueryRequest)
        );
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteExchangeCoin(
            @Valid @RequestBody final ExchangeDeleteRequest exchangeDeleteRequest
    ) throws ExchangeRateNotFoundException {

        this.exchangeService.deleteExchange(
                this.exchangeRestParser.convertRequestToDomain(exchangeDeleteRequest)
        );
    }
}
