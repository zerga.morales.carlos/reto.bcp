package com.zerga.morales.reto.bcp.exchange.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "exchange")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "coin_source")
    private String coinSource;

    @Column(name = "coin_destiny")
    private String coinDestiny;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "since")
    private LocalDateTime since;

    @Column(name = "enabled")
    private Boolean enabled;
}
