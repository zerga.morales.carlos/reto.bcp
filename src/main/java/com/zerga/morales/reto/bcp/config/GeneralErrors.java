package com.zerga.morales.reto.bcp.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public enum GeneralErrors {

    GEN_ALL_01("Sintaxis inválida en formato, tamaño, tipo de dato, valor determinado o requerido.", HttpStatus.BAD_REQUEST),
    GEN_ALL_02("En este momento no se encuentra disponible.", HttpStatus.INTERNAL_SERVER_ERROR),
    GEN_ALL_03("El recurso al cual se intenta acceder no existe", HttpStatus.NOT_FOUND),
    GEN_ALL_04("El verbo que estas consultando no esta habilitado", HttpStatus.METHOD_NOT_ALLOWED),
    GEN_ALL_05("La unica forma de comunicacion con los servicios es por json", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    GEN_ALL_06("Revise que la peticion sea una peticion json complaint.", HttpStatus.BAD_REQUEST),
    GEN_ALL_07("Revise el contrato y verifique los tipos de datos.", HttpStatus.BAD_REQUEST);

    private final String error;

    private final HttpStatus status;
}
