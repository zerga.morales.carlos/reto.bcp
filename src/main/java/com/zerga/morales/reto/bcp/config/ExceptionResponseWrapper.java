package com.zerga.morales.reto.bcp.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExceptionResponseWrapper {

    private String message;

    private String code;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<DetailResponseWrapper> errors;

    @Builder(builderMethodName = "builderWithGeneralError", builderClassName = "ResponseWrapperGeneralErrorBuilder")
    public ExceptionResponseWrapper(GeneralErrors generalError, @Singular(value = "error") List<DetailResponseWrapper> errors) {
        this.code = generalError.name();
        this.message = generalError.getError();
        this.errors = errors;
    }
}