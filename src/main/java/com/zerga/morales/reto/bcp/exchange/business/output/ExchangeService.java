package com.zerga.morales.reto.bcp.exchange.business.output;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ExchangeRate;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;

public interface ExchangeService {
    Exchange createExchange(Exchange domain);

    void deleteExchange(Exchange domain) throws ExchangeRateNotFoundException;

    ResponseEntityList<Exchange> list(ExchangeQueryRequest exchangeQueryRequest);

    ExchangeRate doRateExchange(ExchangeRate convertRequestToDomain) throws ExchangeRateNotFoundException;
}
