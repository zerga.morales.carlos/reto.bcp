package com.zerga.morales.reto.bcp.exchange.controller;

import com.zerga.morales.reto.bcp.exchange.business.output.ExchangeService;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeRateRequest;
import com.zerga.morales.reto.bcp.exchange.controller.response.ExchangeRateResponse;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/ux/exchange-rate")
@Slf4j
@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
@Validated
public class ExchangeRateRestAdapter {

    private final ExchangeService exchangeService;

    private final ExchangeRestParser exchangeRestParser;

    public ExchangeRateRestAdapter(ExchangeService exchangeService, ExchangeRestParser exchangeRestParser) {
        this.exchangeService = exchangeService;
        this.exchangeRestParser = exchangeRestParser;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ExchangeRateResponse exchangeCoin(
            @Valid @RequestBody final ExchangeRateRequest exchangeRateRequest
    ) throws ExchangeRateNotFoundException {

        return this.exchangeRestParser.convertDomainToResponse(
                this.exchangeService.doRateExchange(
                        this.exchangeRestParser.convertRequestToDomain(exchangeRateRequest)
                )
        );
    }
}
