package com.zerga.morales.reto.bcp.exchange.controller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.Min;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeQueryRequest {

    @Length(min = 3, max = 3)
    private String coinSource;

    @Length(min = 3, max = 3)
    private String coinDestiny;

    @Min(1)
    private Integer pageSize;

    @Min(1)
    private Integer pageNumber;

    public Pageable getPageable() {
        return (pageSize == null || pageNumber == null) ? Pageable.unpaged() : PageRequest.of(pageNumber, pageSize);
    }
}
