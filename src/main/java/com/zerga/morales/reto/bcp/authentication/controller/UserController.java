package com.zerga.morales.reto.bcp.authentication.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.zerga.morales.reto.bcp.authentication.business.domain.User;
import com.zerga.morales.reto.bcp.authentication.controller.request.GenerateTokenRequest;
import com.zerga.morales.reto.bcp.authentication.controller.response.UserResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.validation.Valid;

@RestController
public class UserController {

    private final UserRestParser userRestParser;

    public UserController(UserRestParser userRestParser) {
        this.userRestParser = userRestParser;
    }

    @PostMapping("login")
    public UserResponse login(@Valid @RequestBody GenerateTokenRequest request) {

        User user = this.userRestParser.convertRequestToDomain(request);
        String token = getJWTToken(user.getValue());
        return this.userRestParser.convertDomainToResponse(user, token);

    }

    private String getJWTToken(String value) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setSubject(value)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .claim("user", value)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
