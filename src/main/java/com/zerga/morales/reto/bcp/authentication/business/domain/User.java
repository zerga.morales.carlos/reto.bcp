package com.zerga.morales.reto.bcp.authentication.business.domain;

import com.zerga.morales.reto.bcp.authentication.controller.request.GenerateTokenRequest;
import com.zerga.morales.reto.bcp.authentication.controller.response.UserResponse;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Data
@Builder
public class User {

    private String user;

    private String password;

    public String getValue() {
        return String.format("%s:%s", this.user, this.password);
    }

}
