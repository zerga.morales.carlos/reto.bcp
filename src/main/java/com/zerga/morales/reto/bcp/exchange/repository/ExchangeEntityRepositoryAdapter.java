package com.zerga.morales.reto.bcp.exchange.repository;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.business.input.ExchangeClientPort;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ExchangeEntityRepositoryAdapter implements ExchangeClientPort {

    private final ExchangeEntityRepositoryParser exchangeEntityRepositoryParser;

    private final ExchangeEntityRepository exchangeEntityRepository;

    public ExchangeEntityRepositoryAdapter(ExchangeEntityRepositoryParser exchangeEntityRepositoryParser,
                                           ExchangeEntityRepository exchangeEntityRepository) {
        this.exchangeEntityRepositoryParser = exchangeEntityRepositoryParser;
        this.exchangeEntityRepository = exchangeEntityRepository;
    }

    @Override
    public Exchange create(Exchange exchange) {
        ExchangeEntity entity = this.exchangeEntityRepositoryParser.convertDomainToEntity(exchange);
        entity.setEnabled(true);
        return this.exchangeEntityRepositoryParser.convertEntityToDomain(
                this.exchangeEntityRepository.saveAndFlush(
                        entity
                )
        );
    }

    @Override
    public Exchange findLast(Exchange domain) {
        return this.exchangeEntityRepository.findFirstByEnabledTrueAndCoinSourceAndCoinDestinyOrderBySinceDesc(domain.getCoinSource(), domain.getCoinDestiny())
                .map(this.exchangeEntityRepositoryParser::convertEntityToDomain)
                .orElse(null);
    }

    @Override
    public void delete(String id) {
        Optional<ExchangeEntity> optionalEntity = this.exchangeEntityRepository.findById(id);
        if (optionalEntity.isPresent()) {
            ExchangeEntity entity = optionalEntity.get();
            entity.setEnabled(false);
            this.exchangeEntityRepository.saveAndFlush(entity);
        }
    }

    @Override
    public ResponseEntityList<Exchange> list(ExchangeQueryRequest exchangeQueryRequest) {

        return this.exchangeEntityRepositoryParser.convertEntityToDomain(
                this.exchangeEntityRepository.findAll(exchangeQueryRequest.getCoinSource(), exchangeQueryRequest.getCoinDestiny(), exchangeQueryRequest.getPageable())
        );
    }
}
