package com.zerga.morales.reto.bcp.config;

import lombok.Data;

import java.io.IOException;

@Data
public class InvalidTokenException extends IOException {

    private String type;

    public InvalidTokenException(String type) {
        super();
        this.type = type;
    }
}
