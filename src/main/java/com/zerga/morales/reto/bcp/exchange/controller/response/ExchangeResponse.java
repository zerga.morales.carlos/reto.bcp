package com.zerga.morales.reto.bcp.exchange.controller.response;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
public class ExchangeResponse {

    private String coinSource;

    private String coinDestiny;

    private Double rate;

    private LocalDateTime since;
}
