package com.zerga.morales.reto.bcp.exchange.exception;

public abstract class BaseException extends Exception{

    public abstract String getCode();

    public abstract String getError();
}
