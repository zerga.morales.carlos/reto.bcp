package com.zerga.morales.reto.bcp.exchange.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateResponse {

    private Double amount;

    private Double amountWithExchange;

    private Double exchangeRate;

    private String coinSource;

    private String coinDestiny;
}
