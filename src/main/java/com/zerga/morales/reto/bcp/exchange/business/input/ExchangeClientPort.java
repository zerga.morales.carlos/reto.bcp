package com.zerga.morales.reto.bcp.exchange.business.input;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;

public interface ExchangeClientPort {

    Exchange create(Exchange exchange);

    Exchange findLast(Exchange domain);

    void delete(String id);

    ResponseEntityList<Exchange> list(ExchangeQueryRequest exchangeQueryRequest);
}
