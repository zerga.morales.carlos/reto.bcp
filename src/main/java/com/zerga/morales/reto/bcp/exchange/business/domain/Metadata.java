package com.zerga.morales.reto.bcp.exchange.business.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Metadata {

    private Integer pageSize;

    private Integer pageNumber;

    private Integer count;
}
