package com.zerga.morales.reto.bcp.authentication.controller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenerateTokenRequest {

    @NotNull
    @Length(min = 3)
    private String user;

    @NotNull
    @Length(min = 3)
    private String password;
}
