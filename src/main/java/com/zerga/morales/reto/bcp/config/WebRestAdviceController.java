package com.zerga.morales.reto.bcp.config;

import com.fasterxml.jackson.core.json.UTF8StreamJsonParser;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.zerga.morales.reto.bcp.exchange.exception.BaseException;
import io.jsonwebtoken.ExpiredJwtException;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_01;
import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_02;
import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_04;
import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_05;
import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_06;
import static com.zerga.morales.reto.bcp.config.GeneralErrors.GEN_ALL_07;

@RestControllerAdvice
public class WebRestAdviceController {

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToBadRequestResponseEntity(HttpMessageNotReadableException exception) {

        if (exception.getCause() instanceof MismatchedInputException) {
            List<DetailResponseWrapper> validationsWrappers = Collections.singletonList(
                    DetailResponseWrapper.builder()
                            .field(((UTF8StreamJsonParser) ((MismatchedInputException) exception.getCause()).getProcessor()).getParsingContext().getCurrentName())
                            .message(exception.getMessage().split(";")[0])
                            .build()
            );
            return this.convertToGeneralErrorResponseEntity(GEN_ALL_07, validationsWrappers);
        } else {
            return this.convertToGeneralErrorResponseEntity(GEN_ALL_06);
        }
    }

    @ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToHttpMediaTypeNotSupportedResponseEntity() {
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_05);
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToHttpRequestMethodNotSupportedResponseEntity() {
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_04);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToInternalServerErrorResponseEntity(MethodArgumentTypeMismatchException exception) {
        List<DetailResponseWrapper> validationsWrappers = Collections.singletonList(
                DetailResponseWrapper.builder()
                        .field(exception.getName())
                        .message("Field data type is different than expected")
                        .build()
        );
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_01, validationsWrappers);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertConstraintViolationExceptionToResponseEntity(ConstraintViolationException exception) {

        List<DetailResponseWrapper> validationsWrappers = new ArrayList<>();
        for (ConstraintViolation<?> error :
                exception.getConstraintViolations()) {
            validationsWrappers.add(DetailResponseWrapper.builder()
                    .field(((PathImpl) error.getPropertyPath()).getLeafNode().toString())
                    .message(error.getMessage())
                    .build());
        }
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_01, validationsWrappers);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertMethodArgumentNotValidExceptionToResponseEntity(MethodArgumentNotValidException exception) {

        List<DetailResponseWrapper> validationsWrappers = new ArrayList<>();
        for (ObjectError objectError :
                exception.getBindingResult().getAllErrors()) {
            validationsWrappers.add(DetailResponseWrapper.builder()
                    .field(objectError instanceof FieldError ? ((FieldError) objectError).getField() : objectError.getObjectName())
                    .message(objectError.getDefaultMessage())
                    .build());
        }
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_01, validationsWrappers);
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToInvalidTokenExceptionResponseEntity(ExpiredJwtException invalidTokenException) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                ExceptionResponseWrapper.builder()
                        .code("EXPIRED_JWT")
                        .message(invalidTokenException.getMessage())
                        .build()
        );
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToInternalServerErrorResponseEntity() {
        return this.convertToGeneralErrorResponseEntity(GEN_ALL_02);
    }

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ExceptionResponseWrapper> convertToBaseExceptionResponseEntity(BaseException baseException) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(
                ExceptionResponseWrapper.builder()
                        .code(baseException.getCode())
                        .message(baseException.getError())
                        .build()
        );
    }

    private ResponseEntity<ExceptionResponseWrapper> convertToGeneralErrorResponseEntity(GeneralErrors generalError) {
        return this.convertToGeneralErrorResponseEntity(generalError, new ArrayList<>());
    }

    private ResponseEntity<ExceptionResponseWrapper> convertToGeneralErrorResponseEntity(GeneralErrors generalError, List<DetailResponseWrapper> detailedList) {
        return ResponseEntity.status(generalError.getStatus()).body(
                ExceptionResponseWrapper.builderWithGeneralError()
                        .generalError(generalError)
                        .errors(detailedList)
                        .build()
        );
    }
}
