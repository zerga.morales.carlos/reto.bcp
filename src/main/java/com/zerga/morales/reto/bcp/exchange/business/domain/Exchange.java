package com.zerga.morales.reto.bcp.exchange.business.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Exchange {

    private String id;

    private String coinSource;

    private String coinDestiny;

    private Double exchangeRate;

    private LocalDateTime exchangeDateTime;
}
