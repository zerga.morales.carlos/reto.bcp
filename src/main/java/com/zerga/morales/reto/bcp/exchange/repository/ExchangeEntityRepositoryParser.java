package com.zerga.morales.reto.bcp.exchange.repository;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.Metadata;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ExchangeEntityRepositoryParser {
    public Exchange convertEntityToDomain(ExchangeEntity exchangeEntity) {
        return Optional.ofNullable(exchangeEntity)
                .map(element -> Exchange.builder()
                        .id(element.getId())
                        .exchangeRate(element.getRate())
                        .coinDestiny(element.getCoinDestiny())
                        .coinSource(element.getCoinSource())
                        .exchangeDateTime(element.getSince())
                        .build())
                .orElse(null);
    }

    public ExchangeEntity convertDomainToEntity(Exchange exchange) {
        return Optional.ofNullable(exchange)
                .map(element -> ExchangeEntity.builder()
                        .id(element.getId())
                        .rate(element.getExchangeRate())
                        .coinDestiny(element.getCoinDestiny())
                        .coinSource(element.getCoinSource())
                        .since(element.getExchangeDateTime())
                        .build())
                .orElse(null);
    }

    public ResponseEntityList<Exchange> convertEntityToDomain(Page<ExchangeEntity> all) {
        return ResponseEntityList.<Exchange>builder()
                .metadata(
                        Metadata.builder()
                                .pageSize(all.getSize())
                                .pageNumber(all.getTotalPages())
                                .count((int) all.getTotalElements())
                                .build()
                )
                .result(this.convertEntityToDomain(all.getContent()))
                .build();
    }

    private List<Exchange> convertEntityToDomain(List<ExchangeEntity> content) {
        return Optional.ofNullable(content)
                .orElse(Collections.emptyList())
                .stream()
                .map(this::convertEntityToDomain)
                .collect(Collectors.toList());
    }
}
