package com.zerga.morales.reto.bcp.exchange.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ExchangeEntityRepository extends JpaRepository<ExchangeEntity, String> {

    Optional<ExchangeEntity> findFirstByEnabledTrueAndCoinSourceAndCoinDestinyOrderBySinceDesc(String coinSource, String coinDestiny);

    @Query("SELECT ee FROM ExchangeEntity ee WHERE ee.enabled = true AND (ee.coinDestiny = :coinSource or :coinSource is null) AND (ee.coinDestiny = :coinDestiny or :coinDestiny is null)")
    Page<ExchangeEntity> findAll(final @Param("coinSource") String coinSource, final @Param("coinDestiny") String coinDestiny, Pageable pageable);
}
