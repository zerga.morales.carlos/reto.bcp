package com.zerga.morales.reto.bcp.exchange.business;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ExchangeRate;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.business.input.ExchangeClientPort;
import com.zerga.morales.reto.bcp.exchange.business.output.ExchangeService;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ExchangeRateServiceImpl implements ExchangeService {

    private final ExchangeClientPort exchangeClientPort;

    public ExchangeRateServiceImpl(ExchangeClientPort exchangeClientPort) {
        this.exchangeClientPort = exchangeClientPort;
    }

    @Override
    public Exchange createExchange(Exchange domain) {

        domain.setExchangeDateTime(LocalDateTime.now());

        return this.exchangeClientPort.create(domain);
    }

    @Override
    public void deleteExchange(Exchange domain) throws ExchangeRateNotFoundException {

        Exchange previousExchange = this.exchangeClientPort.findLast(domain);

        if (previousExchange == null) {
            throw new ExchangeRateNotFoundException(domain);
        }
        this.exchangeClientPort.delete(previousExchange.getId());
    }

    @Override
    public ResponseEntityList<Exchange> list(ExchangeQueryRequest exchangeQueryRequest) {
        return this.exchangeClientPort.list(exchangeQueryRequest);
    }

    @Override
    public ExchangeRate doRateExchange(ExchangeRate domain) throws ExchangeRateNotFoundException {
        Exchange persistedExchange = this.exchangeClientPort.findLast(domain.getExchange());
        if (persistedExchange == null) {
            throw new ExchangeRateNotFoundException(domain.getExchange());
        }
        domain.setExchange(persistedExchange);

        domain.setAmountWithExchange(domain.getAmount() * persistedExchange.getExchangeRate());
        return domain;
    }
}
