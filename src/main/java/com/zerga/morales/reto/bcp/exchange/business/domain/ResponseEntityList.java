package com.zerga.morales.reto.bcp.exchange.business.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ResponseEntityList<T> {

    private List<T> result;

    private Metadata metadata;
}
