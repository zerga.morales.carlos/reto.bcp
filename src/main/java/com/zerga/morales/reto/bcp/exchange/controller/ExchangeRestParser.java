package com.zerga.morales.reto.bcp.exchange.controller;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ExchangeRate;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeCreateRequest;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeDeleteRequest;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeRateRequest;
import com.zerga.morales.reto.bcp.exchange.controller.response.ExchangeRateResponse;
import com.zerga.morales.reto.bcp.exchange.controller.response.ExchangeResponse;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ExchangeRestParser {

    public Exchange convertRequestToDomain(ExchangeCreateRequest exchangeCreateRequest) {
        return Optional.ofNullable(exchangeCreateRequest)
                .map(element -> Exchange.builder()
                        .exchangeRate(element.getRate())
                        .coinDestiny(element.getCoinDestiny())
                        .coinSource(element.getCoinSource())
                        .build())
                .orElse(null);
    }

    public ExchangeResponse convertDomainToResponse(Exchange exchange) {
        return Optional.ofNullable(exchange)
                .map(element -> ExchangeResponse.builder()
                        .rate(element.getExchangeRate())
                        .coinDestiny(element.getCoinDestiny())
                        .coinSource(element.getCoinSource())
                        .since(element.getExchangeDateTime())
                        .build())
                .orElse(null);
    }

    public Exchange convertRequestToDomain(ExchangeDeleteRequest exchangeDeleteRequest) {
        return Optional.ofNullable(exchangeDeleteRequest)
                .map(element -> Exchange.builder()
                        .coinDestiny(element.getCoinDestiny())
                        .coinSource(element.getCoinSource())
                        .build())
                .orElse(null);
    }

    public ResponseEntityList<ExchangeResponse> convertDomainToResponse(ResponseEntityList<Exchange> list) {
        return Optional.ofNullable(list)
                .map(element -> ResponseEntityList.<ExchangeResponse>builder()
                        .metadata(element.getMetadata())
                        .result(this.convertDomainToResponse(element.getResult()))
                        .build())
                .orElse(null);
    }

    private List<ExchangeResponse> convertDomainToResponse(List<Exchange> result) {
        return Optional.ofNullable(result)
                .orElse(Collections.emptyList())
                .stream()
                .map(this::convertDomainToResponse)
                .collect(Collectors.toList());
    }

    public ExchangeRate convertRequestToDomain(ExchangeRateRequest exchangeRateRequest) {
        return Optional.ofNullable(exchangeRateRequest)
                .map(element -> ExchangeRate.builder()
                        .amount(element.getAmount())
                        .exchange(Exchange.builder()
                                .coinDestiny(element.getCoinDestiny())
                                .coinSource(element.getCoinSource())
                                .build())
                        .build())
                .orElse(null);
    }

    public ExchangeRateResponse convertDomainToResponse(ExchangeRate exchangeRate) {
        return Optional.ofNullable(exchangeRate)
                .map(element -> ExchangeRateResponse.builder()
                        .amount(element.getAmount())
                        .amountWithExchange(element.getAmountWithExchange())
                        .exchangeRate(element.getExchange().getExchangeRate())
                        .coinSource(element.getExchange().getCoinSource())
                        .coinDestiny(element.getExchange().getCoinDestiny())
                        .build())
                .orElse(null);
    }
}
