package com.zerga.morales.reto.bcp.exchange.repository;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.Metadata;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeEntityRepositoryParserTest {

    @InjectMocks
    private ExchangeEntityRepositoryParser exchangeEntityRepositoryParser;

    @Test
    public void testConvertEntityToDomain_ShouldReturnExchangeResponseEntityList_WhenParsingExchangeEntityPage() {

        ExchangeEntity element = ExchangeEntity.builder()
                .id("ID")
                .rate(1.0)
                .coinDestiny("PEN")
                .coinSource("USD")
                .since(LocalDateTime.of(2020, 12, 12, 12, 12, 12))
                .build();
        Page<ExchangeEntity> all = new PageImpl<>(Collections.singletonList(element), PageRequest.of(1, 1), 1);

        ResponseEntityList<Exchange> expected = ResponseEntityList.<Exchange>builder()
                .metadata(
                        Metadata.builder()
                                .pageSize(all.getSize())
                                .pageNumber(all.getTotalPages())
                                .count((int) all.getTotalElements())
                                .build()
                )
                .result(Collections.singletonList(
                        Exchange.builder()
                                .id(element.getId())
                                .exchangeRate(element.getRate())
                                .coinDestiny(element.getCoinDestiny())
                                .coinSource(element.getCoinSource())
                                .exchangeDateTime(element.getSince())
                                .build()
                ))
                .build();

        ResponseEntityList<Exchange> parsed = this.exchangeEntityRepositoryParser.convertEntityToDomain(all);

        assertEquals(expected, parsed);
    }

    @Test
    public void testConvertDomainToEntity_ShouldReturnExchangeEntity_WhenParsingExchange() {
        Exchange exchange = Exchange.builder()
                .exchangeRate(1.0)
                .coinDestiny("PEN")
                .coinSource("USD")
                .exchangeDateTime(LocalDateTime.of(2020, 12, 12, 12, 12, 12))
                .build();

        ExchangeEntity expected = ExchangeEntity.builder()
                .rate(exchange.getExchangeRate())
                .coinDestiny(exchange.getCoinDestiny())
                .coinSource(exchange.getCoinSource())
                .since(exchange.getExchangeDateTime())
                .build();

        ExchangeEntity parsed = this.exchangeEntityRepositoryParser.convertDomainToEntity(exchange);

        assertEquals(expected, parsed);
    }

}