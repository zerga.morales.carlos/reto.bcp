package com.zerga.morales.reto.bcp.exchange.controller.request;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeQueryRequestTest {

    @Test
    public void testGetPageable_ShouldReturnPageable_WithoutZero() {

        ExchangeQueryRequest exchangeQueryRequest = ExchangeQueryRequest.builder()
                .pageSize(1)
                .pageNumber(1)
                .build();
        Pageable pageable = PageRequest.of(1, 1);

        assertEquals(pageable, exchangeQueryRequest.getPageable());
    }



}