package com.zerga.morales.reto.bcp.exchange.repository;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeEntityRepositoryAdapterTest {

    @InjectMocks
    private ExchangeEntityRepositoryAdapter exchangeEntityRepositoryAdapter;

    @Mock
    private ExchangeEntityRepositoryParser exchangeEntityRepositoryParser;

    @Mock
    private ExchangeEntityRepository exchangeEntityRepository;

    @Test
    public void testCreate_ShouldReturnExchange_WhenCreateExchange() {

        Exchange exchange = mock(Exchange.class),
                exchangePersisted = mock(Exchange.class);
        ExchangeEntity exchangeEntity = mock(ExchangeEntity.class),
                persistedExchangeEntity = mock(ExchangeEntity.class);

        when(this.exchangeEntityRepositoryParser.convertDomainToEntity(eq(exchange))).thenReturn(exchangeEntity);
        doNothing().when(exchangeEntity).setEnabled(eq(true));
        when(this.exchangeEntityRepository.saveAndFlush(eq(exchangeEntity))).thenReturn(persistedExchangeEntity);
        when(this.exchangeEntityRepositoryParser.convertEntityToDomain(eq(persistedExchangeEntity))).thenReturn(exchangePersisted);

        Exchange processed = this.exchangeEntityRepositoryAdapter.create(exchange);

        verify(this.exchangeEntityRepositoryParser, times(1)).convertDomainToEntity(eq(exchange));
        verify(exchangeEntity, times(1)).setEnabled(eq(true));
        verify(this.exchangeEntityRepository, times(1)).saveAndFlush(eq(exchangeEntity));
        verify(this.exchangeEntityRepositoryParser, times(1)).convertEntityToDomain(eq(persistedExchangeEntity));

        assertEquals(exchangePersisted, processed);
    }

    @Test
    public void testFindLast_ShouldReturnExchange_WhenSearchExchange() {

        Exchange exchange = mock(Exchange.class),
                persistedExchange = mock(Exchange.class);

        String coinSource = "COIN",
                coinDestiny = "COIN";

        ExchangeEntity persistedExchangeEntity = mock(ExchangeEntity.class);

        Optional<ExchangeEntity> exchangeEntityOptional = Optional.of(persistedExchangeEntity);

        when(exchange.getCoinSource()).thenReturn(coinSource);
        when(exchange.getCoinDestiny()).thenReturn(coinDestiny);
        when(this.exchangeEntityRepository.findFirstByEnabledTrueAndCoinSourceAndCoinDestinyOrderBySinceDesc(eq(coinSource), eq(coinDestiny))).thenReturn(exchangeEntityOptional);
        when(this.exchangeEntityRepositoryParser.convertEntityToDomain(eq(persistedExchangeEntity))).thenReturn(persistedExchange);

        Exchange processed = this.exchangeEntityRepositoryAdapter.findLast(exchange);

        verify(exchange, times(1)).getCoinSource();
        verify(exchange, times(1)).getCoinDestiny();
        verify(this.exchangeEntityRepository, times(1)).findFirstByEnabledTrueAndCoinSourceAndCoinDestinyOrderBySinceDesc(eq(coinSource), eq(coinDestiny));
        verify(this.exchangeEntityRepositoryParser, times(1)).convertEntityToDomain(eq(persistedExchangeEntity));

        assertEquals(persistedExchange, processed);
    }

    @Test
    public void testDelete_ShouldReturnNothing_WhenDeleteById() {

        String id = "ID";

        ExchangeEntity exchangeEntity = mock(ExchangeEntity.class);
        Optional<ExchangeEntity> optionalEntity = Optional.of(exchangeEntity);

        when(this.exchangeEntityRepository.findById(eq(id))).thenReturn(optionalEntity);
        doNothing().when(exchangeEntity).setEnabled(eq(false));
        when(this.exchangeEntityRepository.saveAndFlush(eq(exchangeEntity))).thenReturn(exchangeEntity);

        this.exchangeEntityRepositoryAdapter.delete(id);

        verify(this.exchangeEntityRepository, times(1)).findById(eq(id));
        verify(exchangeEntity, times(1)).setEnabled(eq(false));
        verify(this.exchangeEntityRepository, times(1)).saveAndFlush(eq(exchangeEntity));

    }

    @Test
    public void testDelete_ShouldReturnNothing_WhenDoNothing() {

        String id = "ID";

        Optional<ExchangeEntity> optionalEntity = Optional.empty();

        when(this.exchangeEntityRepository.findById(eq(id))).thenReturn(optionalEntity);

        this.exchangeEntityRepositoryAdapter.delete(id);

        verify(this.exchangeEntityRepository, times(1)).findById(eq(id));
    }

    @Test
    public void testList_ShouldReturnExchangeResponseEntityList_WhenExchangeQueryRequest() {

        ExchangeQueryRequest exchangeQueryRequest = mock(ExchangeQueryRequest.class);

        Page<ExchangeEntity> exchangeEntityPage = mock(Page.class);
        ResponseEntityList<Exchange> exchangeResponseEntityList = mock(ResponseEntityList.class);

        String coinDestiny = "COIN",
                coinSource = "COIN";

        Pageable pageable = mock(Pageable.class);

        when(exchangeQueryRequest.getCoinDestiny()).thenReturn(coinDestiny);
        when(exchangeQueryRequest.getCoinSource()).thenReturn(coinSource);
        when(exchangeQueryRequest.getPageable()).thenReturn(pageable);
        when(this.exchangeEntityRepository.findAll(eq(coinSource), eq(coinDestiny), eq(pageable))).thenReturn(exchangeEntityPage);
        when(this.exchangeEntityRepositoryParser.convertEntityToDomain(eq(exchangeEntityPage))).thenReturn(exchangeResponseEntityList);

        ResponseEntityList<Exchange> processed = this.exchangeEntityRepositoryAdapter.list(exchangeQueryRequest);

        verify(exchangeQueryRequest, times(1)).getCoinDestiny();
        verify(exchangeQueryRequest, times(1)).getCoinSource();
        verify(exchangeQueryRequest, times(1)).getPageable();
        verify(this.exchangeEntityRepository, times(1)).findAll(eq(coinSource), eq(coinDestiny), eq(pageable));
        verify(this.exchangeEntityRepositoryParser, times(1)).convertEntityToDomain(eq(exchangeEntityPage));

        assertEquals(exchangeResponseEntityList, processed);
    }

}