package com.zerga.morales.reto.bcp.exchange.controller;

import com.zerga.morales.reto.bcp.exchange.business.domain.ExchangeRate;
import com.zerga.morales.reto.bcp.exchange.business.output.ExchangeService;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeRateRequest;
import com.zerga.morales.reto.bcp.exchange.controller.response.ExchangeRateResponse;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateRestAdapterTest {

    @InjectMocks
    private ExchangeRateRestAdapter exchangeRateRestAdapter;

    @Mock
    private ExchangeService exchangeService;

    @Mock
    private ExchangeRestParser exchangeRestParser;

    @Test
    public void testExchangeCoin_ShouldReturnExchangeRateResponse_WhenDoingExchange() throws ExchangeRateNotFoundException {

        ExchangeRateRequest exchangeRateRequest = mock(ExchangeRateRequest.class);
        ExchangeRate exchangeRate = mock(ExchangeRate.class);
        ExchangeRateResponse exchangeRateResponse = mock(ExchangeRateResponse.class);

        when(this.exchangeRestParser.convertRequestToDomain(eq(exchangeRateRequest))).thenReturn(exchangeRate);
        when(this.exchangeService.doRateExchange(eq(exchangeRate))).thenReturn(exchangeRate);
        when(this.exchangeRestParser.convertDomainToResponse(eq(exchangeRate))).thenReturn(exchangeRateResponse);

        ExchangeRateResponse processed = this.exchangeRateRestAdapter.exchangeCoin(exchangeRateRequest);

        verify(this.exchangeRestParser, times(1)).convertRequestToDomain(eq(exchangeRateRequest));
        verify(this.exchangeService, times(1)).doRateExchange(eq(exchangeRate));
        verify(this.exchangeRestParser, times(1)).convertDomainToResponse(eq(exchangeRate));

        assertEquals(exchangeRateResponse, processed);
    }
}