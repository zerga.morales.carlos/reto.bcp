package com.zerga.morales.reto.bcp.exchange.business;

import com.zerga.morales.reto.bcp.exchange.business.domain.Exchange;
import com.zerga.morales.reto.bcp.exchange.business.domain.ExchangeRate;
import com.zerga.morales.reto.bcp.exchange.business.domain.ResponseEntityList;
import com.zerga.morales.reto.bcp.exchange.business.input.ExchangeClientPort;
import com.zerga.morales.reto.bcp.exchange.controller.request.ExchangeQueryRequest;
import com.zerga.morales.reto.bcp.exchange.exception.ExchangeRateNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceImplTest {

    @InjectMocks
    private ExchangeRateServiceImpl exchangeRateService;

    @Mock
    private ExchangeClientPort exchangeClientPort;

    @Test
    public void testCreateExchange_ShouldReturnExchange_WhenCreateNewExchange() {

        Exchange exchange = mock(Exchange.class),
                persistedExchange = mock(Exchange.class);

        doNothing().when(exchange).setExchangeDateTime(any(LocalDateTime.class));
        when(this.exchangeClientPort.create(eq(exchange))).thenReturn(persistedExchange);

        Exchange processed = this.exchangeRateService.createExchange(exchange);

        verify(exchange, times(1)).setExchangeDateTime(any(LocalDateTime.class));
        verify(this.exchangeClientPort, times(1)).create(eq(exchange));

        assertEquals(persistedExchange, processed);
    }

    @Test
    public void testDeleteExchange_ShouldReturnNothing_WhenDeleteOldExchange() throws ExchangeRateNotFoundException {

        String exchangeId = "EXCHANGE_ID";

        Exchange exchange = mock(Exchange.class),
                persistedExchange = mock(Exchange.class);

        when(this.exchangeClientPort.findLast(eq(exchange))).thenReturn(persistedExchange);
        when(persistedExchange.getId()).thenReturn(exchangeId);
        doNothing().when(this.exchangeClientPort).delete(eq(exchangeId));

        this.exchangeRateService.deleteExchange(exchange);

        verify(this.exchangeClientPort, times(1)).findLast(eq(exchange));
        verify(persistedExchange, times(1)).getId();
        verify(this.exchangeClientPort, times(1)).delete(eq(exchangeId));

    }

    @Test(expected = ExchangeRateNotFoundException.class)
    public void testDeleteExchange_ShouldThrowExchangeRateNotFoundException_WhenDeleteNotFound() throws ExchangeRateNotFoundException {

        Exchange exchange = mock(Exchange.class),
                persistedExchange = null;

        when(this.exchangeClientPort.findLast(eq(exchange))).thenReturn(persistedExchange);

        this.exchangeRateService.deleteExchange(exchange);
    }

    @Test
    public void testList_ShouldReturnExchangeList_WhenSearchByExchangeQueryRequest() {

        ExchangeQueryRequest exchangeQueryRequest = mock(ExchangeQueryRequest.class);
        ResponseEntityList<Exchange> exchangeResponseEntityList = mock(ResponseEntityList.class);

        when(this.exchangeClientPort.list(eq(exchangeQueryRequest))).thenReturn(exchangeResponseEntityList);

        ResponseEntityList<Exchange> processed = this.exchangeRateService.list(exchangeQueryRequest);

        verify(this.exchangeClientPort, times(1)).list(eq(exchangeQueryRequest));

        assertEquals(exchangeResponseEntityList, processed);
    }

    @Test
    public void testDoRateExchange_ShouldReturnExchangeRate_WhenDoingExchange() throws ExchangeRateNotFoundException {

        Double unit = 2.0,
                price = 3.5,
                total = 7.0;

        ExchangeRate exchangeRate = mock(ExchangeRate.class);
        Exchange exchange = mock(Exchange.class),
                persistedExchange = mock(Exchange.class);

        when(exchangeRate.getExchange()).thenReturn(exchange);
        when(this.exchangeClientPort.findLast(eq(exchange))).thenReturn(persistedExchange);
        doNothing().when(exchangeRate).setExchange(eq(persistedExchange));
        when(exchangeRate.getAmount()).thenReturn(unit);
        when(persistedExchange.getExchangeRate()).thenReturn(price);
        doNothing().when(exchangeRate).setAmountWithExchange(total);

        ExchangeRate processed = this.exchangeRateService.doRateExchange(exchangeRate);

        verify(exchangeRate, times(1)).getExchange();
        verify(this.exchangeClientPort, times(1)).findLast(eq(exchange));
        verify(exchangeRate, times(1)).setExchange(eq(persistedExchange));
        verify(exchangeRate, times(1)).getAmount();
        verify(persistedExchange, times(1)).getExchangeRate();
        verify(exchangeRate, times(1)).setAmountWithExchange(eq(total));

        assertEquals(exchangeRate, processed);
    }

    @Test(expected = ExchangeRateNotFoundException.class)
    public void testDoRateExchange_ShouldThrowExchangeRateNotFoundException_WhenNoExchange() throws ExchangeRateNotFoundException {

        ExchangeRate exchangeRate = mock(ExchangeRate.class);
        Exchange exchange = mock(Exchange.class),
                persistedExchange = null;

        when(exchangeRate.getExchange()).thenReturn(exchange);
        when(this.exchangeClientPort.findLast(eq(exchange))).thenReturn(persistedExchange);

        this.exchangeRateService.doRateExchange(exchangeRate);
    }
}