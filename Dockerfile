FROM maven:3.6.3-jdk-8 as builder

COPY . /apps
WORKDIR /apps
RUN mvn install

FROM openjdk:8-jdk-alpine

COPY --from=builder /apps/target/ /app/

ENTRYPOINT ["java","-jar","/app/app.jar"]